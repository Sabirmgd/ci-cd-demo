# Package the application

## Test packaging locally 

1. We need to package the application into a docker container

1. Refer to the Dockerfile 

1. To check the port check `hello-cicd\bin\www` file

1. To build the docker image
```
docker build . -t sabirmgd/node-web-app
```

1. To run the image:
```
docker run -p 49160:3000 -d sabirmgd/node-web-app
```

1. Go to the browser and go to: `localhost:49160`

## Integrate packaging in the pipeline